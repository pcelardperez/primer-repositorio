
package boletin8ex1;

import javax.swing.JOptionPane;

/**
 * Implementa unha clase consumo, que forma parte da centralita electrónica dun coche e ten as seguintes características :
Atributos :
km   kilómetros percorridos polo coche
litros  Litros de combustible consumidos
vMedvelocidade media
pGas  Prezo da gasolina
Metodos :
Dous constructores , un con parámetros e outro sen eles, que inicializan os  valores dos atributos
getTempo  Indica o tempo empregado en realizar a viaxe
consumoMedio consumo medio do vehículo ( en litros cada 100 km )
consumoEuros consumo medio( en € cada 100 km )
setKms  modifica o valor dos km
setLitros “             “  “       “    litros                      
setvMed   “            “              vMed
setPGas  “”             “             pGas

 * @author pcelardperez
 */
public class Boletin8ex1 {
    static float kmt, lit, vel, gas;
    
    public static void main(String[] args) {
        
        int confirmado=JOptionPane.showConfirmDialog(null, "Bienvenido, quieres empezar el viaje?");
        do{
        if(confirmado==0){
        String km=JOptionPane.showInputDialog("Introduce los Kilometros que recorriste");
        kmt = Float.parseFloat(km);
        
        String litros=JOptionPane.showInputDialog("Introduce los litros repostados");
        lit = Float.parseFloat(litros);
        
        String vmed=JOptionPane.showInputDialog("Introduce la velocidad media que has tenido");
        vel = Float.parseFloat(vmed);
        
        String pgas=JOptionPane.showInputDialog("Introduce el precio de la gasolina repostada");
        gas = Float.parseFloat(pgas);
        
        metodos dato = new metodos(kmt, lit, vel, gas); 
        
        JOptionPane.showMessageDialog(null,"Para recorrer "+dato.getKm()+" km en "+dato.getTempoh()+ "horas: \nTendrás un consumo medio de "+dato.consumoMedio(kmt, lit)+" litros a los 100km y gastarás "+dato.consumoEuros(lit, kmt, gas)+" €");
        confirmado=JOptionPane.showConfirmDialog(null, "Quieres hacer otro viaje?");
        }
        
        }while(confirmado==0);
   }
}
    

