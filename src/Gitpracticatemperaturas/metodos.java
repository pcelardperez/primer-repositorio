

package Gitpracticatemperaturas;

    //Aqui calculamos todos los datos que mostramos por pantalla en la otra clase
public class metodos {
    private float km, litros, vmed, pgas, tempoh;
    private float consumo, consumoe, cambio;
    static float kmtotales;
    metodos(){
        km=0;
        litros=0;
        vmed=0;
        pgas=0;
    }
    metodos(float km, float litros, float vmed, float pgas){
        this.km=km;
        this.litros=litros;
        this.vmed=vmed;
        this.pgas=pgas;
        
    }
    
    //tenemos sets y gets para acceder a las variables private.
    public void setKm(float km){
        this.km=km;
    }
    public float getKm(){
        return km;
    }
    public void setLitros(float litros){
        this.litros=litros;
    }
    public float getLitros(){
        return litros;
    }
    public void setVmed(float vmed){
        this.vmed=vmed;
    }
    public float getVmed(){
        return vmed;
    }
    public void setPgas(float pgas){
        this.pgas=pgas;
    }
    public float getPgas(){
        return pgas;
    }
    
    public float getTempoh(){
        return tempoh=getVmed()/getKm();
    }
    
    //calculamos el consumo medio
    public float consumoMedio(float km, float litros){
        return consumo= (litros/km)*100;
    }
    
    //calculamos el precio del consumo en €
    public float consumoEuros(float litros, float km, float pgas){
        return consumoe= ((litros/km)*pgas)*100;
    }
    /*
    public float sumarKm(float km, float cambio){
        return kmtotales=km+cambio;
    }
    */
 }



