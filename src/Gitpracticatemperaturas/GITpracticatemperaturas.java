/**
 * v0.1 añadidos tags 2º intento
 */
package Gitpracticatemperaturas;

import javax.swing.JOptionPane;

/**
 * Implementa unha clase consumo, que forma parte da centralita electrónica dun coche e ten as seguintes características :
Atributos :
km   kilómetros percorridos polo coche
litros  Litros de combustible consumidos
vMedvelocidade media
pGas  Prezo da gasolina
Metodos :
getTempo  Indica o tempo empregado en realizar a viaxe
consumoMedio consumo medio do vehículo ( en litros cada 100 km )
consumoEuros consumo medio( en € cada 100 km )
setKms  Modifica o valor dos km
setLitros Modifica o valor dos litros                      
setvMed   modifica o valor dos vMed
setPGas  Modifica o valor do pGas

 * @author pcelardperez
 */
public class GITpracticatemperaturas {
    
    //Declaramos las variables, kilometros, litros, velocidad y gasolina
    static float kmt, lit, vel, gas;
    
    public static void main(String[] args) {
        //Mostramos los mensajes para pedir datos
        int confirmado=JOptionPane.showConfirmDialog(null, "Bienvenido, quieres empezar el viaje?");
        do{
            if(confirmado==0){
            
            //pedimos los km recorridos
        String km=JOptionPane.showInputDialog("Introduce los Kilometros que recorriste");
            kmt = Float.parseFloat(km);
        
            //pedimos los litros repostados
        String litros=JOptionPane.showInputDialog("Introduce los litros repostados");
            lit = Float.parseFloat(litros);
        
            //pedimos la velocidad media que ha tenido
        String vmed=JOptionPane.showInputDialog("Introduce la velocidad media que has tenido");
            vel = Float.parseFloat(vmed);
        
            //pedimos el precio de la gasolina repostada
        String pgas=JOptionPane.showInputDialog("Introduce el precio de la gasolina repostada");
            gas = Float.parseFloat(pgas);
        
        metodos dato = new metodos(kmt, lit, vel, gas); 
            
            //mostramos los datos obtenidos por pantalla
        JOptionPane.showMessageDialog(null,"Para recorrer "+dato.getKm()+" km en "+dato.getTempoh()+ "horas: \nTendrás un consumo medio de "+dato.consumoMedio(kmt, lit)+" litros a los 100km y gastarás "+dato.consumoEuros(lit, kmt, gas)+" €");
        confirmado=JOptionPane.showConfirmDialog(null, "Quieres hacer otro viaje?");
        }
        
        }while(confirmado==0);
   }
}
    

